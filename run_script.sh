source /scratch/cluster/eliasl/miniconda3/etc/profile.d/conda.sh
conda activate dpb
python main_no_ddg.py ./data/cifar.python --dataset cifar10 --arch resnet_no_ddg --epochs 1 --learning_rate 0.01 --schedule 150 225 --gammas 0.1 0.1 --batch_size 128 --manualSeed 2 --depth 110 --splits 4