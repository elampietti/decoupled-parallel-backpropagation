import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np

model_with = [16.295,18.561,18.260,19.942,18.326,18.500,18.651,21.158,24.198,18.528]
model_without = [16.165,18.602,19.327,18.536,18.536,18.632,18.558,18.529,18.468,18.576]
#model_dpb = [21.830,24.276,24.294,24.127,23.915,24.175,24.089,24.088,24.225,24.247]

print(sum(model_without))
print(sum(model_with))
#print(sum(model_dpb))

x_labels = ["Pipeline", "Pipeline+DPB"]
times = [sum(model_without), sum(model_with)]

y_pos = np.arange(len(x_labels))

fig, ax = plt.subplots()

ax.bar(
    x=y_pos,
    height=times
)

plt.xticks(y_pos, x_labels)

# Axis formatting.
ax.spines['top'].set_visible(False)
ax.spines['right'].set_visible(False)
ax.spines['left'].set_visible(False)
ax.spines['bottom'].set_color('#DDDDDD')
ax.tick_params(bottom=False, left=False)
ax.set_axisbelow(True)
ax.yaxis.grid(True, color='#EEEEEE')
ax.xaxis.grid(False)

# Add labels and a title. Note the use of `labelpad` and `pad` to add some
# extra space between the text and the tick labels.
# ax.set_xlabel('Number of GPUs', labelpad=10, color='#333333')
ax.set_ylabel('Total Training Time (Seconds)', labelpad=10, color='#333333')
ax.set_title('Training Time for 10 Epochs', color='#333333',
             weight='bold')

fig.tight_layout()

plt.savefig('barchart_pipeline_ddg.png')