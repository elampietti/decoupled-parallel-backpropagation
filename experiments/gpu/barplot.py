import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np

gpu_2_time = [25.927,25.674,25.728,25.463,25.617,25.631,25.648,25.514,25.597,25.682]
gpu_4_time = [21.830,24.276,24.294,24.127,23.915,24.175,24.089,24.088,24.225,24.247]
gpu_8_time = [21.859,23.980,24.004,23.969,24.070,26.178,24.580,24.080,24.109,24.081]

gpus = [2, 4, 8]
times = [sum(gpu_2_time), sum(gpu_4_time), sum(gpu_8_time)]

y_pos = np.arange(len(gpus))

fig, ax = plt.subplots()

ax.bar(
    x=y_pos,
    height=times
)

plt.xticks(y_pos, gpus)

# Axis formatting.
ax.spines['top'].set_visible(False)
ax.spines['right'].set_visible(False)
ax.spines['left'].set_visible(False)
ax.spines['bottom'].set_color('#DDDDDD')
ax.tick_params(bottom=False, left=False)
ax.set_axisbelow(True)
ax.yaxis.grid(True, color='#EEEEEE')
ax.xaxis.grid(False)

# Add labels and a title. Note the use of `labelpad` and `pad` to add some
# extra space between the text and the tick labels.
ax.set_xlabel('Number of GPUs', labelpad=10, color='#333333')
ax.set_ylabel('Total Training Time (Seconds)', labelpad=10, color='#333333')
ax.set_title('Training Time for 10 Epochs by GPUs', color='#333333',
             weight='bold')

fig.tight_layout()

plt.savefig('barchart.png')