import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

epochs = [0,1,2,3,4,5,6,7,8,9]

gpu_2_acc = [44.709,52.265,60.443,57.595,63.538,68.750,72.706,72.419,77.087,71.499]
gpu_4_acc = [44.007,50.850,59.691,62.925,69.126,71.806,73.685,73.863,72.547,76.315]
gpu_8_acc = [37.955,41.792,50.168,56.260,60.008,59.958,64.211,71.301,73.240,75.188]

# plot lines
plt.plot(epochs, gpu_2_acc, label = "GPUs: 2")
plt.plot(epochs, gpu_4_acc, label = "GPUs: 4")
plt.plot(epochs, gpu_8_acc, label = "GPUs: 8")
plt.legend()

plt.xlabel("Epochs", color='#333333')
plt.ylabel("Top-1 Accuracy (%)", color='#333333')
plt.title("Top-1 Accuracies by GPUs", weight='bold', color='#333333')

plt.grid()


plt.savefig('lineplot.png')