import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

epochs = [0,1,2,3,4,5,6,7,8,9]

#awk '{print $2}' gpu_2.txt

without_dpb = [45.352,59.533,58.208,69.739,70.372,73.368,73.912,75.643,77.423,78.985]
with_dpb = [44.007,50.850,59.691,62.925,69.126,71.806,73.685,73.863,72.547,76.315]

# plot lines
plt.plot(epochs, without_dpb, label = "Without DPB")
plt.plot(epochs, with_dpb, label = "With DPB")
plt.legend()

plt.xlabel("Epochs", color='#333333')
plt.ylabel("Top-1 Accuracy (%)", color='#333333')
plt.title("Top-1 Accuracies", weight='bold', color='#333333')

plt.grid()


plt.savefig('lineplot.png')