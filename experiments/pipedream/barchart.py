import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np

model_without = [52.569,54.646,54.665,54.349,54.207,54.399,54.320,54.572,54.789,54.381]
model_with = [16.165,18.602,19.327,18.536,18.536,18.632,18.558,18.529,18.468,18.576]
model_dpb = [21.830,24.276,24.294,24.127,23.915,24.175,24.089,24.088,24.225,24.247]

print(sum(model_without))
print(sum(model_with))
print(sum(model_dpb))

x_labels = ["Base", "DPB", "Pipeline"]
times = [sum(model_without), sum(model_dpb), sum(model_with)]

y_pos = np.arange(len(x_labels))

fig, ax = plt.subplots()

ax.bar(
    x=y_pos,
    height=times
)

plt.xticks(y_pos, x_labels)

# Axis formatting.
ax.spines['top'].set_visible(False)
ax.spines['right'].set_visible(False)
ax.spines['left'].set_visible(False)
ax.spines['bottom'].set_color('#DDDDDD')
ax.tick_params(bottom=False, left=False)
ax.set_axisbelow(True)
ax.yaxis.grid(True, color='#EEEEEE')
ax.xaxis.grid(False)

# Add labels and a title. Note the use of `labelpad` and `pad` to add some
# extra space between the text and the tick labels.
# ax.set_xlabel('Number of GPUs', labelpad=10, color='#333333')
ax.set_ylabel('Total Training Time (Seconds)', labelpad=10, color='#333333')
ax.set_title('Training Time for 10 Epochs', color='#333333',
             weight='bold')

fig.tight_layout()

plt.savefig('barchart_pipeline_3.png')