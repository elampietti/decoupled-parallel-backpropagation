import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

epochs_without = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None]
epochs_with = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49]

#awk '{print $2}' gpu_2.txt

without_model = [45.352,59.533,58.208,69.739,70.372,73.368,73.912,75.643,77.423,78.985, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None]
with_model = [25.000,32.318,34.246,35.552,34.721,38.677,36.472,42.741,39.933,37.629,39.181,43.532,38.400,45.856,43.602,49.654,49.308,51.187,52.977,53.896,55.350,55.469,57.555,58.010,58.920,58.356,59.869,55.508,60.344,63.054,61.353,63.835,63.835,62.569,65.407,64.537,66.317,66.515,68.592,66.851,66.228,67.603,68.829,69.986,65.961,69.897,70.886,71.964,71.183,71.707]

# plot lines
plt.plot(epochs_without, without_model, label = "No Pipeline")
plt.plot(epochs_with, with_model, label = "Pipeline")
plt.legend()

plt.xlabel("Epochs", color='#333333')
plt.ylabel("Top-1 Accuracy (%)", color='#333333')
plt.title("Top-1 Accuracies", weight='bold', color='#333333')

plt.grid()


plt.savefig('lineplot.png')