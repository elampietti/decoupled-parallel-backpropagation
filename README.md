## Introduction

This repository is the PyTorch code for the paper [Decoupled Parallel Backpropagation with Convergence Guarantee" by Zhouyuan Huo, Bin Gu, Qian Yang and Huang, Heng, ICML 2018.](https://arxiv.org/pdf/1804.10574.pdf)

Modified from the original repo of zhouyuan.huo@pitt.edu

## Environment Setup
1. Login to `nandor-1.cs.utexas.edu`.
2. Download the latest miniconda into your scratch space on Condor:
```
cd /scratch/cluster/${USER}/
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
chmod 755 Miniconda3-latest-Linux-x86_64.sh
./Miniconda3-latest-Linux-x86_64.sh
```
3. Follow through the installation process:
    * `yes` to the license agreement.
    * Install miniconda3 to `/scratch/cluster/${USER}/miniconda3`.
    * `yes` to conda init.
    * `conda config --set auto_activate_base false` to not run `conda` all the time.
    * Do anything else it tells you to do for `.bashrc` if needed.
    * Add `conda`'s path to your `PATH` environment variable. One way of doing this is to add `PATH=${PATH}:/scratch/cluster/${USER}/miniconda3/bin/` to your `~/.profile`.

4. Clone this repository to anywhere in your Condor scratch space.
5. Create conda environment with `conda create -n dpb python=3.6.2`
6. Activate environment with `conda activate dpb`
7. Install compatible version of PyTorch and CUDA with `conda install pytorch==1.0.0 torchvision==0.2.1 cuda100 -c pytorch`

## Dependencies

* Python 3.6.2 (Anaconda)
* PyTorch 1.0.0
* CUDA 10.0

## Run

cd into the repo directory, run: 
```
python main.py ./data/cifar.python --dataset cifar10 --arch resnet_ddg --epochs 1 --learning_rate 0.01 --schedule 150 225 --gammas 0.1 0.1 --batch_size 128 --manualSeed 2 --depth 110 --splits 2
``` 
Since this is an eldar machine, the epochs argument is set to 1 as these machines are not for long running jobs and just for testing.
To utilize more gpus, edit the --splits argument with the number of gpus desired.

**Note that to obtain the speedup in the paper, the code should be run in a machine with 4 GPUs.**
