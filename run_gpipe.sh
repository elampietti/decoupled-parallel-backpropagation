#!/bin/bash

uname -n

conda init bash
source /u/liaojh/miniconda3/etc/profile.d/conda.sh
conda activate gpipe

nvidia-smi

python main.py ./data/cifar.python --dataset cifar10 --arch resnet_ddg --gpipe --epochs 20 --learning_rate 0.01 --schedule 150 225 --gammas 0.1 0.1 --batch_size 128 --manualSeed 0 --depth 110 --splits 1 --ngpu 4 --chunks 4
